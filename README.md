# DreamOC Demo Generator 

## Introduction
This project was designed for the School of Earth & Environment to help them produce
more demos for the DreamOC machine. The software requires netCDF data files to 
create animations. These netCDF files have only been tested from the MPIC model 
to date. The output of the software is a .mp4 video file that can be transferred
 onto the DreamOC machine.


## How to Build
The software is spread across 4 python files. They each much be ran correctly in the right 
order for the video file to be created at the end. 


### Requirements
First install the requrements listed in requirements.txt. They are all Python libraries 
that are needed to run the code.

*pip install pyevtk*  
*pip install netCDF4*  
*pip install imageio*  
*pip install imageio-ffmpeg*  
*pip install vtk*  
*pip install scipy*  

### gridtoVTK.py
This is the first file to be ran. It takes 2 arguments. The first is the folder where
the netCDF files are currently stored. This must be in the same directory as the code.
 The second is the output folder where you want the generated files to go. Again, this 
 must be in in the same directory.
 
 *python gridtoVTK [filename] [oFile]*
 
 If you use the example folders in this depository. Then the command would be:
 
 *python gridtoVTK netCDFData vtrFiles*
 
 
 Please note: That this file has only been formatted to netCDF files where there is a 
 new file for each timestep. If you have a netCDF file with all timesteps together, this
 file will need to be altered.
 
 
### render.py
This is the file that helps you visualise your animation before you create the image and video files. 
This requires 3 arguments. The first is the file location of the .vtr files. The second is the isosurface 
you wish to render. The first is the isovalue you want to render. There are other settings that can be changed 
but the source code must be edited. Please see comments in the file for this.

*python render.py [file] [isosurface] [isovalue]*

If you are using the data from MPIC here would be a reccomended command:

*python render.py vtrFiles hgliq 0.005*

Other isosurfaces could be: (u,v,w,p,q,r,b,hg,hgliq,vol)


**Known Bug:** When first ran, a static image will be shown. Press e to close this and then the animation will commence


### renderPNG.py
This file is used to generate the image files once the settings have been chosen. It has 4 arguments. The first is 
the file location of the .vtr files. The second is the file location where the output images will be saved. This must be 
a premade file in the same directory. The first is the isosurface and the 4th is the isovalue. 

*python renderPNG.py [file] [oFile] [isosurface] [isovalue]*

If you are using the files in this demo, it would run:

*python renderPNG.py vtrFiles pngImages hgliq 0.005*


### stitcher.py
This is the final file. This stitches the images together to fit the form of the DreamOC machine. Once done it 
takes the images and compresses them into a video file which is saved to your documents. It takes 2 arguments. The 
first is the file location of the png images generated in the last step. The second is the location where the new images 
will be saved. These are the images that have been stitched together.

*python stitcher.py [file] [oFile]*

If you are using the files in this demo, it would run: 

*python stitcher.py pngImages combinedOutput*


If you are using specific size images files, different to what I used, these need to be manually changed on line 236