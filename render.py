############################################################
# Sam Ballard       sc16sb                                 #
# -------------------------------------------------------- #
# This program is used to visualise various animations     #
# with different isovalues and isosurfaces. It allows      #
# the user to choose ideal settings before they generated  #
# the actual video & image files                           #
# -------------------------------------------------------- #
# Takes 3 arguaments                                       #
# python render.py [file] [isosurface] [isovalue]          #
#                                                          #     
# File refers to location of vtr files. They are the two   #
# main settings, more settings can be can be configured,   #
# but they are inside the code. Too many options to add    #
# them all to command line arguaments                      #
# -------------------------------------------------------- #
# Further Settings                                         #
#                                                          #
# Number of Countours - line 94                            #
# Scalar Visibility - line 106                             #
# Rotation Speed - line 144                                #
# Number of rotation per step - line 143                   #
# Isosurface colour - line 113                             #
# Zoom value - 131                                         #
############################################################



# Imports
from vtk import *
import numpy as np 
import sys
import time
import os 


# TIMER - Currently commented out. Timer can be added to code to slow or speed up animation if requried.
# class vtkTimerCallback():
    
#     # Initiate timer
#     def __init__(self):
#         self.timer_count = 0

#     def execute(self,obj,event):
#         # Rotate camera keeping focus & render
#         renderer.GetActiveCamera().Azimuth(3.0)
#         iren.GetRenderWindow().Render()
#         self.timer_count += 1


# Get current directory and file location
path = os.getcwd()
data_path = str(sys.argv[1])
mypath = path + '/' + data_path

# Read in and sort the rectilinear grids from specified folder
filenames = []
for file in os.listdir(mypath):
    if file.startswith('grid'):
        filenames.append(file)
filenames.sort()


# Read in isosurface and isovalue
isosurface = str(sys.argv[2])
isovalue = float(sys.argv[3])


#Updates the reader with next file
def update_reader(fname):
    reader.SetFileName('vtrFiles/' + fname)
    reader.CellArrayStatus = ['u','v','w','p','q','r','b','hg','hgliq','vol']
    reader.Modified()
    reader.Update()

# Read the source file, as XML.
reader = vtkXMLRectilinearGridReader()
update_reader(filenames[40])

# Get the output
output = reader.GetOutput()

# Printing basic data
print("The dimensions are: " , output.GetDimensions())
print("The number of points are: " , output.GetNumberOfPoints())


# Contour Filters
plane = vtk.vtkContourFilter()
plane.SetInputDataObject(output)
plane.SetInputArrayToProcess(0,0,0,0,isosurface)

#Set number of contours
plane.SetNumberOfContours(1)

# Set values 
plane.SetValue(0, isovalue)
#plane.GenerateValues(10, [0,1])


# Polydata Mapper
rgridMapper = vtk.vtkPolyDataMapper()
rgridMapper.SetInputConnection(plane.GetOutputPort())

# Scalar Visibilty.. Controls whether to auto color the isosurfaces
rgridMapper.ScalarVisibilityOff()


# Actor
actor = vtk.vtkActor()
actor.SetMapper(rgridMapper)
actor.GetProperty().SetOpacity(1)
actor.GetProperty().SetColor(0.8,0.8,0.8)
actor.RotateX(-90)




# Create the usual rendering stuff.
renderer = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(renderer)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

# Renderer
renderer.AddActor(actor)
renderer.SetBackground(0, 0, 0)
renderer.ResetCamera()
renWin.SetSize(2400, 2400)
renderer.GetActiveCamera().Zoom(0.5)
renWin.Render()

# Initilize
iren.Initialize()
iren.Start()

for fname in filenames:
    #Update the file name
    update_reader(fname)


    for i in range (50):
        renderer.GetActiveCamera().Azimuth(-0.3)
        iren.GetRenderWindow().Render()
        
    iren.GetRenderWindow().Render()
    
