############################################################
# Sam Ballard       sc16sb                                 #
# ---------------------------------------------------------#
# This program takes a folder of netCDF files and converts #
# them to rectinilear grid files (.vtr).                   #
# ---------------------------------------------------------#
# Takes 1 arguament                                        #
# python gridtoVTK.py [filename] [oFile]                   #
#                                                          #
# The file with the data must be in the same directory.    #
# filename is the folder with netCDF. oFile is outputFile  #
############################################################


# Imports 
from pyevtk.hl import gridToVTK 
import numpy as np 
from netCDF4 import Dataset 
import time
import os
import sys
from os import listdir
from os.path import isfile, join


# Get the current directory and append on location of data files from input arguament
path = os.getcwd()
data_path = str(sys.argv[1])
mypath = path + '/' + data_path

oFile = str(sys.argv[2])

# Add the netCDF files to an array and sort. 
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
onlyfiles.sort()


# Counter starts at 100 to save order with higher numbers
counter = 100

# Loop for every netCDF file. .vtr file generated for each file
for f in onlyfiles:

    # Working file
    print('File: %s' %f)

    # Timer 
    start_time = time.time()

    # open netCDF file for reading.
    filepath = data_path + '/' + str(f)
    ncfile = Dataset(filepath, 'r')
    

    # Read in dimensions (x,y,z) to numpy arrays
    x = np.array(ncfile.variables['x'])[:]
    y = np.array(ncfile.variables['y'])[:]
    z = np.array(ncfile.variables['z'])[:]


    # Read in variables to numpy arrays
    new_u = np.array(ncfile.variables['u'])[0,:,:,:]
    new_v = np.array(ncfile.variables['v'])[0,:,:,:]
    new_w = np.array(ncfile.variables['w'])[0,:,:,:]
    new_p = np.array(ncfile.variables['p'])[0,:,:,:]
    new_q = np.array(ncfile.variables['q'])[0,:,:,:]
    new_r = np.array(ncfile.variables['r'])[0,:,:,:]
    new_b = np.array(ncfile.variables['b'])[0,:,:,:]
    new_hg = np.array(ncfile.variables['hg'])[0,:,:,:]
    new_hgliq = np.array(ncfile.variables['hgliq'])[0,:,:,:]
    new_vol = np.array(ncfile.variables['vol'])[0,:,:,:]

    # Read in the variables (This is the old way of doing it... VERY SLOW)
    #################################################################################
    # u_data = ncfile.variables['u']
    # v_data = ncfile.variables['v']
    # w_data = ncfile.variables['w']
    # p_data = ncfile.variables['p']
    # q_data = ncfile.variables['q']
    # r_data = ncfile.variables['r']
    # b_data = ncfile.variables['b']
    # hg_data = ncfile.variables['hg']
    # hgliq_data = ncfile.variables['hgliq']
    # vol_data = ncfile.variables['vol']


    # #Create numpy arrays 
    # new_u = np.empty([32,32,33])
    # new_v = np.empty([32,32,33])
    # new_w = np.empty([32,32,33])
    # new_p = np.empty([32,32,33])
    # new_q = np.empty([32,32,33])
    # new_r = np.empty([32,32,33])
    # new_b = np.empty([32,32,33])
    # new_hg = np.empty([32,32,33])
    # new_hgliq = np.empty([32,32,33])
    # new_vol = np.empty([32,32,33])

    # # Import the data, removing the first dimensions (time)
    # for k in range(33):
    #     print(k)
    #     for j in range(32):
    #         for i in range(32): 
    #             new_u[i,j,k] = u_data[0,i,j,k]
    #             new_v[i,j,k] = v_data[0,i,j,k]
    #             new_w[i,j,k] = w_data[0,i,j,k]
    #             new_p[i,j,k] = p_data[0,i,j,k]
    #             new_q[i,j,k] = q_data[0,i,j,k]
    #             new_r[i,j,k] = r_data[0,i,j,k]
    #             new_b[i,j,k] = b_data[0,i,j,k]
    #             new_hg[i,j,k] = hg_data[0,i,j,k]
    #             new_hgliq[i,j,k] = hgliq_data[0,i,j,k]
    #             new_vol[i,j,k] = vol_data[0,i,j,k]

    # ################################################################################

    # Create an output name with counter added to keep order
    fileOutputName = 'grid' + str(counter)

    # Output a rectilinear grid file with dimensions and point data inclueded
    gridToVTK(oFile + "/" + fileOutputName, x, y, z, pointData = {"u" : new_u, "v": new_v, "w": new_w, "p": new_p, "q": new_q, "vr": new_r, "b": new_b, "hg": new_hg, "hgliq": new_hgliq, "vol": new_vol})

    # Print computation time
    print(" %s seconds ---" % (time.time() - start_time))
    counter = counter + 1