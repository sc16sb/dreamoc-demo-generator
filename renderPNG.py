##############################################################
# Sam Ballard       sc16sb                                   #
# ---------------------------------------------------------- #
# This program outputs .png files for each stage of the      #
# animation. It is reccomended to use render.py first to     #
# to get the right settings.                                 #
# ---------------------------------------------------------- #
# Takes 4 arguaments                                         #
# python renderPNG.py [file] [oFile] [isosurface] [isovalue] #
#                                                            #     
# File refers to location of vtr files. oFile is the where   #
# you want the image files to be saved to. If you changed    #
# any advanced options in render.py then they will need to   #
# be applied here too.                                       #
##############################################################



# Imports
from vtk import *
import numpy as np 
import sys
import time
import os


# Get current directory and file location
path = os.getcwd()
data_path = str(sys.argv[1])
mypath = path + '/' + data_path

# Read in and sort the rectilinear grids from specified folder
filenames = []
for file in os.listdir(mypath):
    if file.startswith('grid'):
        filenames.append(file)
filenames.sort()


# Gather arguaments 
savePath = str(sys.argv[2])
isosurface = str(sys.argv[3])
isovalue = float(sys.argv[4])


#Updates the reader with next file
def update_reader(fname):
    reader.SetFileName(data_path + '/' + fname)
    reader.CellArrayStatus = ['u','v','w','p','q','r','b','hg','hgliq','vol']
    reader.Modified()
    reader.Update()

# Read the source file, as XML.
reader = vtkXMLRectilinearGridReader()

# Read a file to set the camera at reasonable position
update_reader(filenames[40])

# Get the output
output = reader.GetOutput()

# Printing basic data
print("The dimensions are: " , output.GetDimensions())
print("The number of points are: " , output.GetNumberOfPoints())

# Contour Filters
plane = vtk.vtkContourFilter()
plane.SetInputDataObject(output)
plane.SetInputArrayToProcess(0,0,0,0,isosurface)

#Set number of contours
plane.SetNumberOfContours(1)

# Set values 
plane.SetValue(0, isovalue)
#plane.GenerateValues(10, [0,1])


# Polydata Mapper
rgridMapper = vtk.vtkPolyDataMapper()
rgridMapper.SetInputConnection(plane.GetOutputPort())

# Scalar Visibilty.. Controls whether to auto color the isosurfaces
rgridMapper.ScalarVisibilityOff()

# Actor
actor = vtk.vtkActor()
actor.SetMapper(rgridMapper)
actor.GetProperty().SetOpacity(1)
actor.GetProperty().SetColor(0.8,0.8,0.8)

# Set the cloud upright
actor.RotateX(-90)
actor.RotateZ(90)

# Create the renderer
renderer = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(renderer)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

# Renderer
renderer.AddActor(actor)
renderer.SetBackground(0, 0, 0)
renderer.ResetCamera()
renWin.SetSize(2400, 2400)
renderer.GetActiveCamera().Zoom(0.5)
renWin.Render()

# Output image name
pngname = "outputImg"

# Initiate counter for output names
counter = 100
positions = ['L','C','R']
x=0

# Repeated 3 times for each viewpoint of the clouds (L,C,R)
for x in range(3):

    # Update for each timestep
    for fname in filenames:
        #Update the file name
        update_reader(fname)
        #Image writer filter


        # Rotating the cloud a # of times for each stage
        for i in range (5):

            # Rotate camera angle, keeping focus
            renderer.GetActiveCamera().Azimuth(-0.3)

            # Window to image
            imageWrite = vtk.vtkWindowToImageFilter()
            imageWrite.SetInput(renWin)
            imageWrite.SetScale(1)
            imageWrite.ReadFrontBufferOff()
            imageWrite.Update

            # Png writer
            writer = vtk.vtkPNGWriter()
            writer.SetFileName(savePath + '/' + pngname + str(counter) + positions[x] + '.png')
            counter = counter + 1
            writer.SetInputConnection(imageWrite.GetOutputPort())
            writer.Write()
            print("Rotating %i" %i)

            

        # Window to image
        imageWrite = vtk.vtkWindowToImageFilter()
        imageWrite.SetInput(renWin)
        imageWrite.SetScale(1)
        imageWrite.ReadFrontBufferOff()
        imageWrite.Update

        # Png writer
        writer = vtk.vtkPNGWriter()
        writer.SetFileName(savePath + '/' + pngname + str(counter) + positions[x] + '.png')
        counter = counter + 1
        writer.SetInputConnection(imageWrite.GetOutputPort())
        writer.Write()
        print("FINISHED: " + fname)

        iren.GetRenderWindow().Render()

    # Rotate the actor 
    actor.RotateZ(-90)
    renderer = vtk.vtkRenderer()
    renWin = vtk.vtkRenderWindow()
    renWin.AddRenderer(renderer)
    iren = vtk.vtkRenderWindowInteractor()
    iren.SetRenderWindow(renWin)

    # Renderer
    renderer.AddActor(actor)
    renderer.SetBackground(0, 0, 0)
    renderer.ResetCamera()
    renWin.SetSize(2400, 2400)
    renderer.GetActiveCamera().Zoom(0.5)
    renWin.Render()

    # Reset the counter for the next point of view of the cloud
    counter = 100
    

iren.Initialize()
iren.Start()