############################################################
# Sam Ballard       sc16sb                                 #
# -------------------------------------------------------- #
# This program was based and adapted from Dr Leif Denbys   #
# orginal DreamOC stitcher. The program works by first     #
# reading in all images and combining into DreamOC layout. #
# Once done, the images are combined to a .mp4 video       #
# -------------------------------------------------------- #
# Takes 2 arguaments                                       #
# python stitcher.py [file] [oFile]                        #
#                                                          #     
# File refers to location of image files. The oFile is     #
# location of where the combined images should be saved.   #
############################################################



# Imports
from scipy.constants import pi
import numpy as np
import imageio
import os
import sys

class docStich():
    def __init__(self, w=1920, h=1080, dpi=200):
        # Set default width, height and dpi of the image
        self.w = w
        self.h = h
        self.dpi = dpi

        # Check image size 
        assert w > h

        # calculate centers of zones L, C, R (left, center, right)
        halfWidth = w//2
        quarterWidth = w//4

        # Calculate centers of imgaes
        self.centers = [('L', (w/4, w/2), -pi/2),
                        ('C', (halfWidth, quarterWidth), 0.),
                        ('R', (3.*w/4, w/2), pi/2)]


        self.halfWidth = halfWidth
        self.quarterWidth = quarterWidth

        
        # to insert image at right place need to transpose, clip and do a asked insert into final image

        # Create array size of w and h
        single_width_array = np.arange(w)
        single_height_array = np.arange(h)
        
        # Makes coordinate arrays
        multi_width_array, multi_height_array = np.meshgrid(single_width_array, single_height_array, indexing='ij')

    
        # Checks height array > width array AND width array > width/2
        self.l_template = np.logical_and(multi_height_array > multi_width_array, multi_width_array < w/2)

        # Checks height array > (width - width array) AND width array > width/2
        self.r_template = np.logical_and(multi_height_array > (w - multi_width_array), multi_width_array > w/2)

        # Checks NOT L_mask AND NOT R_mask
        self.c_template = np.logical_and(
            np.logical_not(self.l_template), np.logical_not(self.r_template)
        )
        

    # Centers the image in right place
    def _center_image(self, img):

        w = self.w
        h = self.h

        
        w_in, h_in, n_colors = img.shape

        halfWidth = self.halfWidth
        quarterWidth = self.quarterWidth

        # ensure we have a minimum an image the size of the output
        w_min = max(w_in, w)
        h_min = max(h_in, h)
        
        # Blank array ready for image
        img_ = np.ones((w_min, h_min, n_colors)).astype(img.dtype)
        img_[:w_in, :h_in] = img
    
        # center on 0,0
        img_ = np.roll(np.roll(img_, -w_in//2, axis=0), -h_in//2, axis=1)

        # center on the C's center
        img_ = np.roll(np.roll(img_, halfWidth, axis=0), quarterWidth, axis=1)

        # crop to the final image's extent
        img_ = img_[:w,:h]


        return img_


    # Place the center image
    def place_central_image(self, img, img_output):
        c_template = self.c_template
        
        # Roll image down 150 pixels
        img = np.roll(img, -150, axis = 1)

        # Create centered blank canvas
        img_ = self._center_image(img)

        # Apply image
        img_output[c_template] = img_[c_template]

        return img_output


    # Place the left image
    def placeLeft(self, img, img_output):
        h = self.h
        l_template = self.l_template

        # Roll image down 150 pixels
        img = np.roll(img, 150, axis = 0)

        # Create centered blank canvas
        img_ = self._center_image(img)

        # Rotate 90 degrees anti-clock
        img_ = np.rot90(img_, axes=(1,0), k=1)[:,:h]
        
        # need to crop the mask before we apply it
        m = l_template[:h]

        # Apply image
        img_output[:h][m] = img_[m]

        return img_output

    # Place the right image
    def placeRight(self, img, img_output):
        w = self.w
        h = self.h
        r_template = self.r_template

        # Roll image down 150 pixels
        img = np.roll(img, -150, axis = 0)

        # Create centered blank canvas
        img_ = self._center_image(img)

        # Rotate 90 degrees clock
        img_ = np.rot90(img_, axes=(1,0), k=-1)
        w_ = img_.shape[0]

        # Temp image
        img_temp = np.zeros_like(img_output)
        img_temp[:w_] = img_[:w_,:h]
        img_temp = np.roll(img_temp, axis=0, shift=w-w_)

        # Apply image
        img_output[r_template] = img_temp[r_template]

        return img_output

    # Read the image from file
    def _read_img(self, filename):
        # Read in image from filename
        filepath = imgFile + '/' + filename
        img_array = imageio.imread(filepath)

        # Image is flipped to keep consistant indexing (x-y from bottom right corner)
        return np.flip(np.transpose(img_array, axes=(1,0,2)), axis=1)

    
    def __call__(self, fn_leftImage, fn_rightImage, fn_centerImage):
        leftImage = self._read_img(fn_leftImage)
        rightImage = self._read_img(fn_rightImage)
        centerImage = self._read_img(fn_centerImage)

        # Set width and height
        w = self.w
        h = self.h

        # Takes the final arguament from the shape (colours)
        n_colors = leftImage.shape[-1]
        
        # Check images are the same
        assert leftImage.shape[-1] == rightImage.shape[-1] == centerImage.shape[-1]

        # Create the final image output, start as blank 
        img_output = 255*np.ones((w, h, n_colors)).astype(leftImage.dtype)

        # Place the images one by one
        img_output = self.placeLeft(leftImage, img_output)
        img_output = self.place_central_image(centerImage, img_output)
        img_output = self.placeRight(rightImage, img_output)


        return img_output


if __name__ == "__main__":
   
    # Get the file locations
    imgFile = str(sys.argv[1])
    outFile = str(sys.argv[2])

    # Import the image address into a list and sort
    path = os.getcwd()
    mypath = path + '/' + imgFile
    onlyfiles = []
    for file in os.listdir(mypath):
        if file.startswith('output'):
            onlyfiles.append(file)
    onlyfiles.sort()


    # Number of images that will be created
    numberOfImages = len(onlyfiles) // 3 


    # Number of images being joined
    print("Joining {} images into {} images...\n\n".format(len(onlyfiles),numberOfImages))

    # To keep track of the images that are being joined
    positionCounter = 0

    # For loop to join the images 
    for x in range(numberOfImages):
        # Run the stitcher on the 3 images
        
        # If you are using different/specific images this needs to be customised
        stitcher = docStich(w=1920, h=1080, dpi=200)
        img_output = stitcher(fn_leftImage=onlyfiles[positionCounter+1], fn_centerImage=onlyfiles[positionCounter], fn_rightImage=onlyfiles[positionCounter+2])

        # Print the 3 files being joined
        print("Left image: {}".format(onlyfiles[positionCounter+1]))
        print("Center image: {}".format(onlyfiles[positionCounter]))
        print("Right image: {}".format(onlyfiles[positionCounter+2]))

        # Move position counter along to the next group of images
        positionCounter = positionCounter + 3

        # Add frame number to end of file to keep order
        outputFile = outFile + '/combined' + str(x+1000) + '.png'
        imageio.imwrite(outputFile, np.rot90(img_output, k=1))

        # Print to terminal 
        print("Wrote combined image to {}\n\n".format(outputFile))


    # VIDEO WRITER
    fileList = []
    
    #Add images to list ready to create video
    path2 = path + '/' + outFile + '/'
    for file in os.listdir(path2):
        if file.startswith('combined'):
            complete_path = path2 + file
            fileList.append(complete_path)
    
    

    # Sorts the list in order and creates video
    writer = imageio.get_writer('dreamOC.mp4', fps=8)
    fileList.sort()
    for im in fileList:
        writer.append_data(imageio.imread(im))
    writer.close()

    

